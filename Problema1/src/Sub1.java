
public class Sub1 {
}


class Card{
    private int number;
    private String name;

    public Card(){

    }

    public Card(int number, String name){
        this.number = number;
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class Account{
    private Card card;
    private int id;

    public Account(){}

    public Account(int id, Card card){
        this.id = id;
        this.card = card;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

class Transaction{
    private int amount;
    private Account account1;
    private Account account2;

    public Transaction(){

    }

    public Transaction(int amount, Account account1, Account account2){
        this.amount = amount;
        this.account1 = account1;
        this.account2 = account2;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Account getAccount1() {
        return account1;
    }

    public void setAccount1(Account account1) {
        this.account1 = account1;
    }

    public Account getAccount2() {
        return account2;
    }

    public void setAccount2(Account account2) {
        this.account2 = account2;
    }
}

class User{
    private String name;
    private Account[] personalAccounts;

    public User(){
    }

    public User(String name, Account[] personalAccounts){
        this.name = name;
        this.personalAccounts = personalAccounts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Account[] getPersonalAccounts() {
        return personalAccounts;
    }

    public void setPersonalAccounts(Account[] personalAccounts) {
        this.personalAccounts = personalAccounts;
    }
}

class Bank{
    private Account[] accounts;
    private int bankId;
    private String bankName;

    public Bank(){}

    public Bank(int bankId, String bankName, Account[] accounts){
        this.bankId = bankId;
        this.bankName = bankName;
        this.accounts = accounts;

    }

    public Account[] getAccounts() {
        return accounts;
    }

    public void setAccounts(Account[] accounts) {
        this.accounts = accounts;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}