import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.*;


public class Main extends JFrame{
    JTextField textField;
    JTextArea textArea;
    JButton button;

    Main(){
        setTitle("Subiect 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500,500);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);
        int width = 80; int height = 20;

        textField = new JTextField();
        textField.setBounds(70,10,width,height);

        textArea = new JTextArea();
        textArea.setBounds(70,120,200,200);

        button = new JButton("Insert");
        button.setBounds(70,50,width,height);
        button.addActionListener(new TratareButtonInsert());

        add(textField);
        add(textArea);
        add(button);
    }


    public static void main(String[] args) {
        new Main();
    }

    class TratareButtonInsert implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String textDeIntrodus = Main.this.textField.getText();
            Main.this.textArea.append(textDeIntrodus + "\n");
            Main.this.textField.setText("");
        }
    }
}